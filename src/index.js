import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'
//import AppNavbar from './components/AppNavbar'

//React.StrictMode highlights any potential errors

/*
const name = "Chad"

const user = {
  firstName: "Thor",
  lastName: "Odinson"
}

function formatName(user) {
  return user.firstName + ' ' + user.lastName
}


  // We can also user an arrow funtion (from ES6 update) to create the function

  // const formatName = (user) => `${user.firstName} ${user.lastName}`

const element = <h1>Hello, {formatName(user)}</h1>

ReactDOM.render(
  element,
  document.getElementById('root')
);
*/

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>, 
  document.getElementById('root')
);
